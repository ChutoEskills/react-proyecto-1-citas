# React Proyecto 1 - Citas 
Sistema para agendar citas de mascotas en un veterinario

[Link to Web](https://practical-rosalind-99d204.netlify.app/)

## ¿Herramientas utilizadas?
- React_uuid
- useEffect
- useState
- Formulario básico
- Skeleton css 
- Normalize css
- Google Fonts

## ¿Cómo funciona?
- Se recoge la información mediante el formulario, en caso de ser incorrecta se emite una alerta, cuando es correcta se envía la información por lo que se actualiza el useState "citas" y con ello el div que contiene todas las citas, al momento de que se clicka el botón para eliminar alguna cita, se filtra por el UUID y se mantienen todas aquellas cuyo UUID no es similar al que fue clickado.


## ¿Dudas?
